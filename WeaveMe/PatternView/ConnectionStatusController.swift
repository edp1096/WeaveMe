//
//  ConnectionStatusController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/19/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This helper class listens to that status and uses that to update a status label,
//  and the labeling of a Connect/Disconnect/Cancel button.
//

import UIKit

class ConnectionStatusController: NSObject, KSBluetoothManagerStateListener {
    
    weak var delegate: ConnectionStatusControllerDelegate?
    
    private weak var connectionButtonItem: UIBarButtonItem!
    
    init(buttonItem: UIBarButtonItem) {
        connectionButtonItem = buttonItem
        super.init()

        connectionButtonItem.possibleTitles = [ "Connect", "Cancel", "Disconnect" ]
        connectionButtonItem.action = #selector(ConnectionStatusController.buttonTapped)
        connectionButtonItem.target = self
        KSBluetoothManager.shared.addStateChangeListener(listener: self)
        updateConnectionButton(connectionState: KSBluetoothManager.shared.connectionState)
    }
    
    deinit {
        KSBluetoothManager.shared.removeStateChangeListener(listener: self)
    }
    
    @objc private func buttonTapped(_ sender: Any) {
        NSLog("Connect button pressed")
        switch KSBluetoothManager.shared.connectionState {
        case .notConnected:
            KSBluetoothManager.shared.scanForPeripherals()
        case .scanning:
            KSBluetoothManager.shared.disconnect()
        case .connected:
            KSBluetoothManager.shared.disconnect()
        default:
            NSLog("Unknown connection state \(KSBluetoothManager.shared.connectionState)")
            assertionFailure()
        }
    }
    
    /// When you manually want to force an update of the label/button text/state based on the
    /// current bluetooth connection status call this.
    func updateBasedOnCurrentStatus() {
        updateConnectionStateLabel(connectionState: KSBluetoothManager.shared.connectionState)
        updateConnectionButton(connectionState: KSBluetoothManager.shared.connectionState)
    }
    
    func updateConnectionStateLabel(connectionState: KSPeripheralConnectionState) {
        switch connectionState {
        case .notConnected:
            delegate?.connectionStatusController(self, didChangeStatusText: "Not Connected")
        case .scanning:
            delegate?.connectionStatusController(self, didChangeStatusText: "Scanning")
        case .connected:
            delegate?.connectionStatusController(self, didChangeStatusText: "Connected")
        default:
            delegate?.connectionStatusController(self, didChangeStatusText: "Unknown")
        }
    }
    
    func updateConnectionButton(connectionState: KSPeripheralConnectionState) {
        switch connectionState {
        case .notConnected:
            connectionButtonItem.title = "Connect"
        case .scanning:
            connectionButtonItem.title = "Cancel"
        case .connected:
            connectionButtonItem.title = "Disconnect"
        default:
            connectionButtonItem.title = "--"
        }
    }
    
    // MARK: - KSBluetoothManagerStateListener
    func connectionStateChanged(_ connectionState: KSPeripheralConnectionState) {
        // NSLog("State change %@", connectionState)
        updateConnectionButton(connectionState: connectionState)
        updateConnectionStateLabel(connectionState: connectionState)
    }
    
}
