//
//  WarpState.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 5/11/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This is just some data about the state of all the layers of the loom.  These can be used to
//  display the current state of the loom, and can reflect the current physical state of the loom, or
//  some virtual state.

import UIKit

class WarpState {

    enum LayerState: Int, CustomStringConvertible {
        case liftsWithPlusLever = 0
        case liftsWithMinusLever = 1
        case unknown = 2

        var description: String {
            switch self {
            case .liftsWithPlusLever:
                return "forward"
            case .liftsWithMinusLever:
                return "back"
            case .unknown:
                return "unknown"
            }
        }
    }

    var layerStates: [LayerState] = []
    var isCurrentPhysicalState = false

}
