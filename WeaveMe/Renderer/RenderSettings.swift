//
//  RenderSettings.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/8/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is a struct passed to the renderer to tell it what parts of a pattern to
//  render, the style to use, and the size to make the output image.

import UIKit

struct RenderSettings: Equatable {
    
    // The rectangle to render in pattern x/y coordinate space.
    var rect: CGRect
    
    // The style to use during rendering.
    var style: PatternRenderer.RenderStyle
    
    // The output width of the image in points.
    var outputImageWidth: CGFloat
    
    static func == (lhs: RenderSettings, rhs: RenderSettings) -> Bool {
        return
            lhs.rect == rhs.rect &&
                lhs.style == rhs.style &&
                lhs.outputImageWidth == rhs.outputImageWidth
    }
}
