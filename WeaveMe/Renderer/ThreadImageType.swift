//
//  ThreadImageType.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/30/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  When rendering we use some basic images that define the shapes of the thread as
//  it passes though the weave.  D indicates Down and U means Up, so a thread that
//  is DDDHoriz is going across horizontally for three spaces at the bottom.
//  DUDHoriz would go from down to up and then down again.  etc.
//
//  This enum is just a concise way for the render to refer to these things and
//  for the image cache to know which shape is desired and what it's base image name is.
//

import Foundation

enum ThreadImageType: Int, CustomStringConvertible {
    case dddHoriz = 0
    case dduHoriz = 1
    case dudHoriz = 2
    case duuHoriz = 3
    case uddHoriz = 4
    case uduHoriz = 5
    case uudHoriz = 6
    case uuuHoriz = 7

    case dddVert = 8
    case dduVert = 9
    case dudVert = 10
    case duuVert = 11
    case uddVert = 12
    case uduVert = 13
    case uudVert = 14
    case uuuVert = 15

    var imageName: String {
        switch self {
        case .dddHoriz:
            return "DDDHoriz"
        case .dduHoriz:
            return "DDUHoriz"
        case .dudHoriz:
            return "DUDHoriz"
        case .duuHoriz:
            return "DUUHoriz"
        case .uddHoriz:
            return "UDDHoriz"
        case .uduHoriz:
            return "UDUHoriz"
        case .uudHoriz:
            return "UUDHoriz"
        case .uuuHoriz:
            return "UUUHoriz"

        case .dddVert:
            return "DDDVert"
        case .dduVert:
            return "DDUVert"
        case .dudVert:
            return "DUDVert"
        case .duuVert:
            return "DUUVert"
        case .uddVert:
            return "UDDVert"
        case .uduVert:
            return "UDUVert"
        case .uudVert:
            return "UUDVert"
        case .uuuVert:
            return "UUUVert"

        }
    }

    var description: String {
        return "\(self.imageName)"
    }
}
