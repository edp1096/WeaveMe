//
//  PatternEveryOtherWarpWeftPlainLayer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/24/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  I have this pattern where if every other warp/weft thread is
//  a much thinner thread than the main "Pattern" thread it can act
//  as a way to hold material together regardless of what the "Pattern"
//  thread is doing.  This can in theory support any float length since
//  the actual float length is limited to 3 in warp and 5 in weft.
//
//  The pattern is essentially plain wave on every other wert row,
//  and an alternating 3/1 -- 1/3 pattern on every third weft row.
//
//  I'm not sure this is useful so I'm pausing production on it.

import UIKit

class PatternEveryOtherWarpWeftPlainLayer: PatternLayer {

    var primaryLayer: PatternLayer? {
        didSet {
            updateUniqueColors()
        }
    }
    
    var plainWarpColor: UIColor = .blue {
        didSet {
            updateUniqueColors()
        }
    }
    var plainWeftColor: UIColor = .white {
        didSet {
            updateUniqueColors()
        }
    }
    
//    private lazy var expandedPlainLayer: PatternLayer = {
//        
//    } ()
    
    override func threadCount(_ type: ThreadType) -> Int {
        guard let primary = primaryLayer else {
            return 0
        }
        
        // We put plain weave on left/right + top/bottom plus in-between every row,
        // so that's n + (n - 1) + 2 or simply this
        return 2*primary.threadCount(type) + 1
    }

    private func updateUniqueColors() {
        var colors: [UIColor] = [plainWarpColor, plainWeftColor]
        if let primaryLayerColors = primaryLayer?.uniqueColors {
            colors.append(contentsOf: primaryLayerColors)
        }

        self.uniqueColors = getUniqueColors(colors)
    }

//    override func isOnTop(_ threadType: ThreadType, x: Int, y: Int) -> Bool {
//        let layerAndY = getPatternLayerAndYValue(for: y)
//        return layerAndY.patternLayer.isOnTop(threadType, x: x, y: layerAndY.y)
//    }
    
    override func setIsOnTop(_ threadType: ThreadType, x: Int, y: Int, value: Bool) {
        assert(false, "PatternEveryOtherWarpWeftPlainLayers can not have their isOnTop set")
    }
    
    // TODO: We may want to always use the primary layer's warp, but for now it makes it
    // more visible if we don't, so for now we honor the warp colors even though they can't really change.
//    override func threadColor(_ threadType: ThreadType, x: Int, y: Int) -> UIColor {
//        let layerAndY = getPatternLayerAndYValue(for: y)
//        return layerAndY.patternLayer.threadColor(threadType, x: x, y: layerAndY.y)
//    }
    
    override func buildFromImage(_ image: UIImage, completion: (Error?) -> Void) {
        assert(false, "PatternMixingLayers can not be set from an image.")
    }
}
