//
//  OverlayLayerBuilderFromImage.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/30/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This takes an image and builds a LayerBuildItem with weftOnTop, and weft colors.
//  For overlay images it is assumed that no warp colors are returned because overlays
//  use whatever warp system is defined in the ground layer.
//
//  Weft rows that have no overlay have the .clear weft color.
//

import UIKit

class OverlayLayerBuilderFromImage {

    /// Takes an image and generates the buildItem for it including weftOnTop, weftColors.
    func buildFromImage(_ image: UIImage, completion: (_ buildItem: LayerBuildItem?, _ error: Error?) -> Void) {
        guard let inputCGImage = image.cgImage else {
            NSLog("Pattern: Unable to get cgImage")
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }

        guard let dataProvider = inputCGImage.dataProvider else {
            NSLog("Pattern: Unable to get data provider for image")
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }

        if inputCGImage.bitsPerPixel != 32 {
            NSLog("Pattern: We currently only support RGBA images.)")
            completion(nil, PatternBuildError.imageIsNotRGBA)
            return
        }

        let pixelData = dataProvider.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelWidth = inputCGImage.width
        let pixelHeight = inputCGImage.height
        let bytesPerPixel = inputCGImage.bitsPerPixel/8

        var pixelIndex: Int = 0

        if pixelWidth < 2 || pixelHeight < 2 {
            NSLog("Pattern: The image is only \(pixelWidth) x \(pixelHeight).  It must be at least 2 x 2.")
            completion(nil, PatternBuildError.imageIsTooSmall)
            return
        }

        var buildItem = LayerBuildItem()

        for weftIndex in 1...pixelHeight - 1 {
            var weftRow: [Bool] = []
            var firstColorInRow: UIColor?
            for warpIndex in 1...pixelWidth - 1 {
                pixelIndex = ((pixelWidth*weftIndex) + warpIndex) * bytesPerPixel
                // Right now we just use the alpha channel being more than half on.
                weftRow.append(data[pixelIndex + 3] > 127)
                if firstColorInRow == nil && data[pixelIndex + 3] > 127 {
                    firstColorInRow = UIColor.init(red: CGFloat(data[pixelIndex])/255.0,
                                                 green: CGFloat(data[pixelIndex + 1])/255.0,
                                                 blue: CGFloat(data[pixelIndex + 2])/255.0,
                                                 alpha: CGFloat(data[pixelIndex + 3])/255.0)
                }
            }
            if let firstRowColor = firstColorInRow {
                buildItem.weftColors.append(firstRowColor)
            } else {
                // Empty rows get a .clear so we have a quick way of
                // checking if the row has any picks on it, and to keep the weft colors as
                // a consistent array indexed to y.
                buildItem.weftColors.append(.clear)
            }
            buildItem.weftOnTop.append(weftRow)
        }

        completion(buildItem, nil)
    }

}
